package com.dji.sdk.sample.flightcontroller;

import android.app.Service;
import android.content.Context;
import android.graphics.Path;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.dji.sdk.sample.R;
import com.dji.sdk.sample.common.DJISampleApplication;
import com.dji.sdk.sample.common.Utils;
import com.dji.sdk.sample.utils.DJIModuleVerificationUtil;
import com.dji.sdk.sample.utils.OnScreenJoystick;
import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoConfig;
import com.google.atap.tangoservice.TangoCoordinateFramePair;
import com.google.atap.tangoservice.TangoErrorException;
import com.google.atap.tangoservice.TangoEvent;
import com.google.atap.tangoservice.TangoOutOfDateException;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import dji.sdk.FlightController.DJIFlightControllerDataType;
import dji.sdk.base.DJIBaseComponent;
import dji.sdk.base.DJIError;

/**
 * Class for virtual stick.
 */
public class VirtualStickView extends RelativeLayout implements View.OnClickListener {

    /**
     * The float array using to represent the location (x, y, z) after take off.
     */
    private final float[] LOCATION_AFTER_TAKE_OFF = new float[]{0, 0, 1.1f};
    /**
     * The scale factor used to scale up the tango tablet to real life data.
     */
    private final int SCALE_FACTOR = 10;

    final float[] cacheAttitude = new float[3];
    final float[] velocity = new float[3];

    private boolean mYawControlModeFlag = true;
    private boolean mRollPitchControlModeFlag = true;
    private boolean mVerticalControlModeFlag = true;
    private boolean mHorizontalCoordinateFlag = true;
    private boolean mStartSimulatorFlag = false;

    private Button mBtnEnableVirtualStick;
    private Button mBtnDisableVirtualStick;
    private Button mBtnHorizontalCoordinate;
    private Button mBtnSetYawControlMode;
    private Button mBtnSetVerticalControlMode;
    private Button mBtnSetRollPitchControlMode;
    private Button mBtnSendCommand;
    private ToggleButton mBtnSimulator;
    private Button mBtnTakeOff;
    private Button control, startLocationRecorder;
    private TextView mTextView;
    private TextView locationData;

    private OnScreenJoystick mScreenJoystickRight;
    private OnScreenJoystick mScreenJoystickLeft;

    private Timer mSendVirtualStickDataTimer;
    private SendVirtualStickDataTask mSendVirtualStickDataTask;

    private float mPitch;
    private float mRoll;
    private float mYaw;
    private float mThrottle;

    private int outputControl = 0;

    /**
     * Tango objects
     */
    private Tango mTango;
    private TangoConfig mConfig;
    private static final String TAG = VirtualStickView.class.getSimpleName();
    private ArrayList<float[]> tangoPoses = new ArrayList<>();

//    private ArrayList<String> lines = new ArrayList<>();
//    Path file = Path.;
//    Files.write();

    /**
     * TangoPoseData converted to real aircraft data.
     */
    private ArrayList<float[]> realFlightLocationData = new ArrayList<>();
    /**
     * Velocity data calculated from the aircraft data.
     */
    private ArrayList<double[]> velocityData = new ArrayList<>();

    /**
     * Current State of the Aircraft.
     */
    private DJIFlightControllerDataType.DJIFlightControllerCurrentState currentState;
    /**
     * Current 3D coordinate of the Aircraft.
     */
    private DJIFlightControllerDataType.DJILocationCoordinate3D coordinate3D;

    /**
     * @param context
     * @param attrs
     */
    public VirtualStickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context, attrs);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (null != mSendVirtualStickDataTimer) {
            mSendVirtualStickDataTask.cancel();
            mSendVirtualStickDataTask = null;
            mSendVirtualStickDataTimer.cancel();
            mSendVirtualStickDataTimer.purge();
            mSendVirtualStickDataTimer = null;
        }
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    mHandler.removeMessages(1);
                case 1:
                    currentState = DJISampleApplication.getAircraftInstance().getFlightController().getCurrentState();
                    coordinate3D = currentState.getAircraftLocation();
                    locationData.setText("Altitude: " + coordinate3D.getAltitude() + "\nLatitude: " + coordinate3D.getLatitude() + "\nLongitude" + coordinate3D.getLongitude());
                    mHandler.sendEmptyMessageDelayed(1, 100);

                    break;

                default:
                    break;
            }

        }

    };
    //mHandler.sendEmptyMessage(1);


    private void initUI(Context context, AttributeSet attrs) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Service.LAYOUT_INFLATER_SERVICE);

        View content = layoutInflater.inflate(R.layout.view_virtual_stick, null, false);
        addView(content, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        mBtnEnableVirtualStick = (Button) findViewById(R.id.btn_enable_virtual_stick);
        mBtnDisableVirtualStick = (Button) findViewById(R.id.btn_disable_virtual_stick);
        mBtnHorizontalCoordinate = (Button) findViewById(R.id.btn_horizontal_coordinate);
        mBtnSetYawControlMode = (Button) findViewById(R.id.btn_yaw_control_mode);
        mBtnSetVerticalControlMode = (Button) findViewById(R.id.btn_vertical_control_mode);
        mBtnSetRollPitchControlMode = (Button) findViewById(R.id.btn_roll_pitch_control_mode);
        mBtnTakeOff = (Button) findViewById(R.id.btn_take_off);
        mBtnSendCommand = (Button)findViewById(R.id.BtnSendCommand);

        /** Button to enable Tango Collecting data. */
        control = (Button) findViewById(R.id.control);
        /** Text to display the location of the aircraft. */
        locationData = (TextView) findViewById(R.id.locationData);
        /** The Button that starts the handler to change the location data. */
        startLocationRecorder = (Button) findViewById(R.id.startLocationRecorder);

        startLocationRecorder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.sendEmptyMessage(1);
            }
        });
        mPitch = mYaw = mRoll = mThrottle =0;

        //DJIFlightControllerDataType.DJIFlightControllerCurrentState currentState = new DJIFlightControllerDataType.DJIFlightControllerCurrentState();
        //DJIFlightControllerDataType.DJILocationCoordinate3D coordinate3D = currentState.getAircraftLocation();

        //locationData.setText("Altitude: " + coordinate3D.getAltitude() + "\nLatitude: " + coordinate3D.getLatitude()+ "\nLongitude" + coordinate3D.getLongitude());

        control.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mTango = new Tango(getContext());
                    mConfig = new TangoConfig();
                    mConfig = mTango.getConfig(TangoConfig.CONFIG_TYPE_CURRENT);
                    mConfig.putBoolean(TangoConfig.KEY_BOOLEAN_MOTIONTRACKING, true);
                    mConfig.putBoolean(TangoConfig.KEY_BOOLEAN_DEPTH, true);

                    final ArrayList<TangoCoordinateFramePair> framePairs = new ArrayList<>();
                    framePairs.add(new TangoCoordinateFramePair(
                            TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                            TangoPoseData.COORDINATE_FRAME_DEVICE));

                    mTango.connectListener(framePairs, new Tango.OnTangoUpdateListener() {
                        @Override
                        public void onPoseAvailable(TangoPoseData tangoPoseData) {
//                            logPose(tangoPoseData);

                            logPositionVelocity(tangoPoseData);

                            outputControl++;

//                            tangoPoses.add(tangoPoseData.getTranslationAsFloats());
//                            currentState = DJISampleApplication.getAircraftInstance().getFlightController().getCurrentState();
//                            coordinate3D = currentState.getAircraftLocation();
//                            float[] current = new float[3];
//                            current[0] = coordinate3D.getAltitude();
//                            current[1] = (float) coordinate3D.getLatitude();
//                            current[2] = (float) coordinate3D.getLongitude();

//                            realFlightLocationData.add(convertFloatArray(tangoPoseData.getTranslationAsFloats(), LOCATION_AFTER_TAKE_OFF));
//                            for (int i = 0; i < tangoPoses.size(); i++) {
//                                Log.d("ArrayList: " + i, Arrays.toString(tangoPoses.get(i)));
//                            }
//                            String res = "";
//                            for (float[] i : tangoPoses) {
//                                res += i + "\t";
//                            }
                            //tangoPoses[0];

//                            for (TangoCoordinateFramePair i: framePairs) {
//                                locationData.setText(mTango.getPoseAtTime(tangoPoseData.timestamp, i).toString());
//                            }
                        }

                        @Override
                        public void onXyzIjAvailable(TangoXyzIjData tangoXyzIjData) {

                        }

                        @Override
                        public void onFrameAvailable(int i) {

                        }

                        @Override
                        public void onTangoEvent(TangoEvent tangoEvent) {

                        }
                    });


                    try {
                        mTango.connect(mConfig);
                    } catch (TangoOutOfDateException e) {
                        // handle the error
                    }

                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        mTango.disconnect();
                        outputControl = 0;
                    } catch (TangoErrorException e) {
                        // handle the error
                    }

                }
                return false;
            }
        });

        mBtnSimulator = (ToggleButton) findViewById(R.id.btn_start_simulator);

        mTextView = (TextView) findViewById(R.id.textview_simulator);

        mScreenJoystickRight = (OnScreenJoystick) findViewById(R.id.directionJoystickRight);
        mScreenJoystickLeft = (OnScreenJoystick) findViewById(R.id.directionJoystickLeft);

        mBtnEnableVirtualStick.setOnClickListener(this);
        mBtnDisableVirtualStick.setOnClickListener(this);
        mBtnHorizontalCoordinate.setOnClickListener(this);
        mBtnSetYawControlMode.setOnClickListener(this);
        mBtnSetVerticalControlMode.setOnClickListener(this);
        mBtnSetRollPitchControlMode.setOnClickListener(this);
        mBtnTakeOff.setOnClickListener(this);
        mBtnSendCommand.setOnClickListener(this);

        //realFlightLocationData = convertToRealLocationData(tangoPoses);


//        while (true) {
//            currentState = DJISampleApplication.getAircraftInstance().getFlightController().getCurrentState();
//            coordinate3D = currentState.getAircraftLocation();
//            locationData.setText("Altitude: " + coordinate3D.getAltitude() + "\nLatitude: " + coordinate3D.getLatitude() + "\nLongitude" + coordinate3D.getLongitude());
//        }


//        mBtnSimulator.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//
//                    mTextView.setVisibility(VISIBLE);
//
//                    DJISampleApplication.getAircraftInstance().getFlightController().getSimulator()
//                            .startSimulator(new DJISimulator.DJISimulatorInitializationData(
//                                            23, 113, 10, 10
//                                    )
//                                    ,new DJIBaseComponent.DJICompletionCallback() {
//                                        @Override
//                                        public void onResult(DJIError djiError) {
//
//                                        }
//                                    });
//                } else {
//
//                    mTextView.setVisibility(INVISIBLE);
//
//                    DJISampleApplication.getAircraftInstance().getFlightController().getSimulator()
//                            .stopSimulator(
//                                    new DJIBaseComponent.DJICompletionCallback() {
//                                        @Override
//                                        public void onResult(DJIError djiError) {
//
//                                        }
//                                    }
//                            );
//                }
//            }
//        });
//
//        DJISampleApplication.getAircraftInstance().getFlightController().getSimulator()
//                .setUpdatedSimulatorStateDataCallback(new DJISimulator.UpdatedSimulatorStateDataCallback() {
//                    @Override
//                    public void onSimulatorDataUpdated(final DJISimulator.DJISimulatorStateData djiSimulatorStateData) {
//                        new Handler(Looper.getMainLooper()).post(new Runnable() {
//
//                            @Override
//                            public void run() {
//                                mTextView.setText("Yaw : " + djiSimulatorStateData.getYaw() + "," + "X : " + djiSimulatorStateData.getPositionX() + "\n" +
//                                        "Y : " + djiSimulatorStateData.getPositionY() + "," +
//                                        "Z : " + djiSimulatorStateData.getPositionZ());
//                            }
//                        });
//                    }
//                });
//
//        mScreenJoystickLeft.setJoystickListener(new OnScreenJoystickListener(){
//
//            @Override
//            public void onTouch(OnScreenJoystick joystick, float pX, float pY) {
//                if(Math.abs(pX) < 0.02 ){
//                    pX = 0;
//                }
//
//                if(Math.abs(pY) < 0.02 ){
//                    pY = 0;
//                }
//                float pitchJoyControlMaxSpeed = DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMaxVelocity;
//                float rollJoyControlMaxSpeed = DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMaxVelocity;
//
//                mPitch = (float)(pitchJoyControlMaxSpeed * pY);
//
//                mRoll = (float)(rollJoyControlMaxSpeed * pX);
//
//                if (null == mSendVirtualStickDataTimer) {
//                    mSendVirtualStickDataTask = new SendVirtualStickDataTask();
//                    mSendVirtualStickDataTimer = new Timer();
//                    mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 100, 200);
//                }
//
//            }
//
//        });
//
//        mScreenJoystickRight.setJoystickListener(new OnScreenJoystickListener() {
//
//            @Override
//            public void onTouch(OnScreenJoystick joystick, float pX, float pY) {
//                if(Math.abs(pX) < 0.02 ){
//                    pX = 0;
//                }
//
//                if(Math.abs(pY) < 0.02 ){
//                    pY = 0;
//                }
//                float verticalJoyControlMaxSpeed = DJIFlightControllerDataType.DJIVirtualStickVerticalControlMaxVelocity;
//                float yawJoyControlMaxSpeed = DJIFlightControllerDataType.DJIVirtualStickYawControlMaxAngularVelocity;
//
//                mYaw = (float)(yawJoyControlMaxSpeed * pX);
//                mThrottle = (float)(verticalJoyControlMaxSpeed * pY);
//
//                if (null == mSendVirtualStickDataTimer) {
//                    mSendVirtualStickDataTask = new SendVirtualStickDataTask();
//                    mSendVirtualStickDataTimer = new Timer();
//                    mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 200);
//                }
//
//            }
//        });
    }

    @Override
    public void onClick(View v) {
        if (!DJIModuleVerificationUtil.isFlightControllerAvailable()) {
            return;
        }
        switch (v.getId()) {
            case R.id.btn_enable_virtual_stick:
                enableVirtualStick();
                break;
            case R.id.btn_disable_virtual_stick:
                disableVirtualStick();
                break;
            case R.id.btn_roll_pitch_control_mode:
                rollPitchMode();
                break;
            case R.id.btn_yaw_control_mode:
                yawControlMode();
                break;
            case R.id.btn_vertical_control_mode:
                verticalControlMode();
                break;
            case R.id.btn_horizontal_coordinate:
                horizontalCoordinate();
                break;
            case R.id.btn_take_off:
                takeOff();
                break;
            case R.id.BtnSendCommand:
                if (null == mSendVirtualStickDataTimer) {
                    mSendVirtualStickDataTask = new SendVirtualStickDataTask();
                    mSendVirtualStickDataTimer = new Timer();
                    mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 100, 200);
                }
            break;
            default:
                break;
        }
    }

    class SendVirtualStickDataTask extends TimerTask {

        @Override
        public void run() {
            DJISampleApplication.getAircraftInstance().getFlightController().setRollPitchControlMode(DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMode.Velocity);
            DJISampleApplication.getAircraftInstance().getFlightController().setYawControlMode(DJIFlightControllerDataType.DJIVirtualStickYawControlMode.Angle);
            DJISampleApplication.getAircraftInstance().getFlightController().setVerticalControlMode(DJIFlightControllerDataType.DJIVirtualStickVerticalControlMode.Velocity);
            DJISampleApplication.getAircraftInstance().getFlightController().setHorizontalCoordinateSystem(DJIFlightControllerDataType.DJIVirtualStickFlightCoordinateSystem.Ground);
            if (DJIModuleVerificationUtil.isFlightControllerAvailable()) {
                DJISampleApplication.getAircraftInstance().
                        getFlightController().sendVirtualStickFlightControlData(
                        new DJIFlightControllerDataType.DJIVirtualStickFlightControlData(
                                mPitch, mRoll, mYaw, mThrottle
                        ), new DJIBaseComponent.DJICompletionCallback() {
                            @Override
                            public void onResult(DJIError djiError) {

                            }
                        }
                );
            }
        }
    }

//    private ArrayList<float[]> convertTangoPoseData(TangoPoseData tangoPoseData) {
//
//    }

//    /**
//     * Convert an ArrayList of TangoPoseData to an ArrayList of realFlightLocationData.
//     */
//    private ArrayList<float[]> convertToRealLocationData(ArrayList<float[]> TangoPoseDataTranslationals) {
//        ArrayList<float[]> resultArrays = new ArrayList<>();
//        for (float[] i : TangoPoseDataTranslationals) {
//            //resultArrays.add(convertFloatArray(i));
//        }
//        return resultArrays;
//    }
//
//    /**
//     * Method that converts the TangoPoseData Translational float array into RealFlightLocationData.
//     *
//     * @param TangoPoseDataTranslational An Array of floats representing the translation data of a TangoPoseData.
//     * @return The converted Array of floats.
//     */
//    private float[] convertFloatArray(float[] TangoPoseDataTranslational, float[] currentPosition) {
//        float[] resultArray = new float[TangoPoseDataTranslational.length];
//        for (int i = 0; i < TangoPoseDataTranslational.length; i++) {
//            resultArray[i] = currentPosition[i] + TangoPoseDataTranslational[i] * SCALE_FACTOR;
//        }
//        return resultArray;
//    }
//
//    /**
//     * Convert an ArrayList of TangoPoseData to velocityData.
//     *
//     * @param TangoPoseDataTranslational
//     * @return
//     */
//    private ArrayList<float[]> getVelocityData(ArrayList<float[]> TangoPoseDataTranslational) {
//
//        ArrayList<float[]> result = new ArrayList<>();
//        return result;
//    }


    private void logPositionVelocity(TangoPoseData pose) {
        /** If we put */
        StringBuilder stringBuilder = new StringBuilder();
        float translation[] = pose.getTranslationAsFloats();
        //mPitch = mYaw = mRoll = mThrottle =0;
////
        if (translation[0] == 0 && translation[1] == 0 && translation[2] == 0) {

            // initial condition.

        } else {
            velocity[0] = (translation[0] - cacheAttitude[0]) * 100f;
            velocity[1] = (translation[1] - cacheAttitude[1]) * 100f;
            velocity[2] = (translation[2] - cacheAttitude[2]) * 100f;
            if (velocity[0]*10 >=0.02)
            {
                mPitch = velocity[0] * 10;
            }


        }
        cacheAttitude[0] = translation[0];
        cacheAttitude[1] = translation[1];
        cacheAttitude[2] = translation[2];
        stringBuilder.append("Position: " +
                String.format("%.2f",translation[0]) + ", " + String.format("%.2f",translation[1]) + ", " + String.format("%.2f",translation[2]) + ", Velocity: "
                + String.format("%.2f",velocity[0]) + ", " +  String.format("%.2f",velocity[1]) + ", " +  String.format("%.2f",velocity[2]) + "\n");
        Log.i(TAG, stringBuilder.toString());
    }

//    /**
//     * Method to print a Log for TangoPoseData.
//     *
//     * @param pose The TangoPoseData that is passed and interpreted.
//     */
//    private void logPose(TangoPoseData pose) {
//        StringBuilder stringBuilder = new StringBuilder();
//
//        float translation[] = pose.getTranslationAsFloats();
//        stringBuilder.append("Position: " +
//                translation[0] + ", " + translation[1] + ", " + translation[2]);
//
//        float orientation[] = pose.getRotationAsFloats();
//        stringBuilder.append(". Orientation: " +
//                orientation[0] + ", " + orientation[1] + ", " +
//                orientation[2] + ", " + orientation[3]);
//
//        Log.i(TAG, stringBuilder.toString());
//    }

    /**
     * Enable Virtual Stick.
     */
    private void enableVirtualStick() {
        DJISampleApplication.getAircraftInstance().
                getFlightController().enableVirtualStickControlMode(
                new DJIBaseComponent.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        Utils.showDialogBasedOnError(getContext(), djiError);
                    }
                }
        );
    }

    /**
     * Disable Virtual Stick
     */
    private void disableVirtualStick() {
        DJISampleApplication.getAircraftInstance().
                getFlightController().disableVirtualStickControlMode(
                new DJIBaseComponent.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        Utils.showDialogBasedOnError(getContext(), djiError);
                    }
                }
        );
    }

    /**
     * Roll Pitch Mode
     */
    private void rollPitchMode() {
        if (mRollPitchControlModeFlag) {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setRollPitchControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMode.Angle);
            mRollPitchControlModeFlag = false;
        } else {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setRollPitchControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickRollPitchControlMode.Velocity
                    );
            mRollPitchControlModeFlag = true;
        }
        try {
            Utils.setResultToToast(getContext(), DJISampleApplication.
                    getAircraftInstance().getFlightController().
                    getRollPitchControlMode().name());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Yaw Control Mode
     */
    private void yawControlMode() {
        if (mYawControlModeFlag) {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setYawControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickYawControlMode.Angle
                    );
            mYawControlModeFlag = false;
        } else {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setYawControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickYawControlMode.AngularVelocity
                    );
            mYawControlModeFlag = true;
        }
        try {
            Utils.setResultToToast(getContext(), DJISampleApplication.
                    getAircraftInstance().getFlightController().
                    getYawControlMode().name());
        } catch (Exception ex) {
        }
        ;

    }

    /**
     * Vertical Control Mode
     */
    private void verticalControlMode() {
        if (mVerticalControlModeFlag) {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setVerticalControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickVerticalControlMode.Position
                    );
            mVerticalControlModeFlag = false;
        } else {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setVerticalControlMode(
                            DJIFlightControllerDataType.DJIVirtualStickVerticalControlMode.Velocity
                    );
            mVerticalControlModeFlag = true;
        }
        try {
            Utils.setResultToToast(getContext(), DJISampleApplication.
                    getAircraftInstance().getFlightController().
                    getVerticalControlMode().name());
        } catch (Exception ex) {
        }
        ;
    }

    /**
     * Take Off
     */
    private void takeOff() {
        DJISampleApplication.getAircraftInstance().getFlightController().takeOff(
                new DJIBaseComponent.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        Utils.showDialogBasedOnError(getContext(), djiError);
                    }
                }
        );
    }

    /**
     * Horizontal Coordinate
     */
    private void horizontalCoordinate() {
        if (mHorizontalCoordinateFlag) {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setHorizontalCoordinateSystem(
                            DJIFlightControllerDataType.DJIVirtualStickFlightCoordinateSystem.Ground
                    );
            mHorizontalCoordinateFlag = false;
        } else {
            DJISampleApplication.getAircraftInstance().getFlightController().
                    setHorizontalCoordinateSystem(
                            DJIFlightControllerDataType.DJIVirtualStickFlightCoordinateSystem.Body
                    );
            mHorizontalCoordinateFlag = true;
        }
        try {
            Utils.setResultToToast(getContext(), DJISampleApplication.
                    getAircraftInstance().getFlightController().
                    getRollPitchCoordinateSystem().name());
        } catch (Exception ex) {
        }
        ;

    }
}
